// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var ParseDashboard = require('parse-dashboard');

var allowInsecureHTTP = -1;
var app = express();



/*var api = new ParseServer({
    databaseURI: 'mongodb://localhost:27017/testa',
    cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
    appId: process.env.APP_ID || 'app',
    masterKey: process.env.MASTER_KEY || 'master', //Add your master key here. Keep it secret!
    serverURL: process.env.SERVER_URL || 'http://localhost:1338/testa', // Don't forget to change to https if needed
    liveQuery: {
        classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
    }
}, { useNewUrlParser: true });*/

var api = new ParseServer({
    databaseURI: 'mongodb://juan:kika123@ds141613.mlab.com:41613/testdata',
    cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
    appId: process.env.APP_ID || 'app_lab',
    masterKey: process.env.MASTER_KEY || 'master', //Add your master key here. Keep it secret!
    serverURL: process.env.SERVER_URL || 'https://ds141613.mlab.com:41613/testdata', // Don't forget to change to https if needed
    liveQuery: {
        classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
    }
}, { useNewUrlParser: true });

/*var config = {
    //YOUR DASHBOARD SETTINGS
    "allowInsecureHTTP": true,
    "apps": [{
        "serverURL": "http://192.168.1.66:1338/test",
        "appId": "app_lab",
        "masterKey": "master",
        "appName": ""
    }],

    "iconsFolder": "icons",
    "users": [{
        "user": "juan",
        "pass": "1"
    }]
};*/

//var dashboard = new ParseDashboard(config, { allowInsecureHTTP: config.allowInsecureHTTP });


app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/test';
app.use(mountPath, api);


//app.use('/dash', dashboard);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
    res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test2', function(req, res) {
    res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = process.env.PORT || 1338;
var httpServer = require('http').createServer(app);

httpServer.listen(port, function() {
    console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);